# Game Closure Devkit Plugin: contactManager

## Usage

Include it in the `manifest.json` file under the "addons" section for your game:

~~~
"addons": [
    "contactmanager"
],
~~~

At the top of your game's `src/Application.js`:

~~~
import contactmanager;
~~~

Now you can use `contactManager.findContact` API:

~~~
contactManager.findContact(bind(this, function(contacts) {
    var contactsArr = JSON.parse(contact);
}), bind(this, function(err) {
    //Log error
}), {
    fields: ["displayName", "name"],
    options : {
        filter : "searchTerm",
        desiredFields: ["displayName", "name", "emails"]
    }
});
~~~

This returns you a list of contacts matching to "searchTerm".

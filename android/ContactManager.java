package com.tealeaf.plugin.plugins;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;

import com.tealeaf.EventQueue;
import com.tealeaf.logger;
import com.tealeaf.plugin.IPlugin;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.google.i18n.phonenumbers.NumberParseException;
import android.telephony.TelephonyManager;


public class ContactManager implements IPlugin {
    class WhereOptions {
        private String where;
        private String[] whereArgs;
        public void setWhere(String where) {
            this.where = where;
        }
        public String getWhere() {
            return where;
        }
        public void setWhereArgs(String[] whereArgs) {
            this.whereArgs = whereArgs;
        }
        public String[] getWhereArgs() {
            return whereArgs;
        }
    }
    public class ContactFound extends com.tealeaf.event.Event {
        String errorMessage;
        String contacts;
        String token;

        public ContactFound() {
            super("contactFound");
            this.errorMessage = "";
            this.token = "";
            this.contacts="";
        }

        public ContactFound(String errorMessage, String contacts, String token) {
            super("contactFound");
            this.errorMessage = errorMessage;
            this.contacts = contacts;
            this.token = token;
        }
    }

    private Activity _activity;        // Activity
    private Context _ctx;            // App context
    private static final Map<String, String> dbMap = new HashMap<String, String>();


    static {
        dbMap.put("id", ContactsContract.Data.CONTACT_ID);
        dbMap.put("displayName", ContactsContract.Contacts.DISPLAY_NAME);
        dbMap.put("name", ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME);
        dbMap.put("name.formatted", ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME);
        dbMap.put("name.familyName", ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME);
        dbMap.put("name.givenName", ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME);
        dbMap.put("name.middleName", ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME);
        dbMap.put("name.honorificPrefix", ContactsContract.CommonDataKinds.StructuredName.PREFIX);
        dbMap.put("name.honorificSuffix", ContactsContract.CommonDataKinds.StructuredName.SUFFIX);
        dbMap.put("nickname", ContactsContract.CommonDataKinds.Nickname.NAME);
        dbMap.put("phoneNumbers", ContactsContract.CommonDataKinds.Phone.NUMBER);
        dbMap.put("phoneNumbers.value", ContactsContract.CommonDataKinds.Phone.NUMBER);
        dbMap.put("emails", ContactsContract.CommonDataKinds.Email.DATA);
        dbMap.put("emails.value", ContactsContract.CommonDataKinds.Email.DATA);
        dbMap.put("addresses", ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS);
        dbMap.put("addresses.formatted", ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS);
        dbMap.put("addresses.streetAddress", ContactsContract.CommonDataKinds.StructuredPostal.STREET);
        dbMap.put("addresses.locality", ContactsContract.CommonDataKinds.StructuredPostal.CITY);
        dbMap.put("addresses.region", ContactsContract.CommonDataKinds.StructuredPostal.REGION);
        dbMap.put("addresses.postalCode", ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE);
        dbMap.put("addresses.country", ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY);
        dbMap.put("ims", ContactsContract.CommonDataKinds.Im.DATA);
        dbMap.put("ims.value", ContactsContract.CommonDataKinds.Im.DATA);
        dbMap.put("organizations", ContactsContract.CommonDataKinds.Organization.COMPANY);
        dbMap.put("organizations.name", ContactsContract.CommonDataKinds.Organization.COMPANY);
        dbMap.put("organizations.department", ContactsContract.CommonDataKinds.Organization.DEPARTMENT);
        dbMap.put("organizations.title", ContactsContract.CommonDataKinds.Organization.TITLE);
        dbMap.put("birthday", ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE);
        dbMap.put("note", ContactsContract.CommonDataKinds.Note.NOTE);
        dbMap.put("photos.value", ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE);
        //dbMap.put("categories.value", null);
        dbMap.put("urls", ContactsContract.CommonDataKinds.Website.URL);
        dbMap.put("urls.value", ContactsContract.CommonDataKinds.Website.URL);
    }

    public ContactManager() {
    }

    public void onCreateApplication(Context applicationContext) {
        //TODO: This needs to be configurable.
        logger.DISABLE_DEBUG = false;
        logger.log("in onCreateApplication method..");
        _ctx = applicationContext;
    }

    public void onCreate(Activity activity, Bundle savedInstanceState) {
        logger.log("in onCreate method..");
        _activity = activity;
    }

    public void onResume() {
        logger.log("in onResume method..");
    }

    public void onStart() {
        logger.log("in onStart method..");
    }

    public void onPause() {
    }

    public void onStop() {
        logger.log("in onStop method..");
    }

    public void onDestroy() {
    }

    public void onNewIntent(Intent intent) {
    }

    public void setInstallReferrer(String referrer) {
    }

    public void onActivityResult(Integer request, Integer result, Intent data) {
    }

    private void showGPSDisabledAlertToUser() {
    }

    public void logError(String error) {
    }

    public void onBackPressed() {
    }

    public void search(String jsonData) {
        logger.log("Searching contacts");
        logger.log("Data:"+ jsonData);
        String token = "";
        try {
            String searchTerm = "";
            int limit = Integer.MAX_VALUE;
            boolean multiple = true;
            JSONObject jsonObj = new JSONObject(jsonData);
            JSONArray fields = jsonObj.optJSONArray("fields");
            JSONObject options = jsonObj.optJSONObject("options");
            token = jsonObj.optString("token");

            logger.log("Options: "+ options.toString());

            if (options != null) {
                searchTerm = options.optString("filter");
                if (searchTerm.length() == 0) {
                    searchTerm = "%";
                }
                else {
                    searchTerm = "%" + searchTerm + "%";
                }

                multiple = options.getBoolean("multiple");
                if (!multiple) {
                    limit = 1;
                }

            }
            else {
                searchTerm = "%";
            }

            logger.log("Search term: "+ searchTerm);
            HashMap<String, Boolean> populate = buildPopulationSet(options);
            logger.log("population set term: "+ populate.toString());
            WhereOptions whereOptions = buildWhereClause(fields, searchTerm);
            Cursor idCursor = _activity.getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                    new String[] { ContactsContract.Data.CONTACT_ID },
                    whereOptions.getWhere(),
                    whereOptions.getWhereArgs(),
                    ContactsContract.Data.CONTACT_ID + " ASC");

            Set<String> contactIds = new HashSet<String>();
            int idColumn = -1;
            while (idCursor.moveToNext()) {
                if (idColumn < 0) {
                    idColumn = idCursor.getColumnIndex(ContactsContract.Data.CONTACT_ID);
                }
                contactIds.add(idCursor.getString(idColumn));
                logger.log("idCursor val: "+ idCursor.getString(idColumn));
            }
            idCursor.close();

            WhereOptions idOptions = buildIdClause(contactIds, searchTerm);
            HashSet<String> columnsToFetch = getColumnsToFetch(populate);

            Cursor c = _activity.getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                    columnsToFetch.toArray(new String[] {}),
                    idOptions.getWhere(),
                    idOptions.getWhereArgs(),
                    ContactsContract.Data.CONTACT_ID + " ASC");

            JSONArray contacts;

            contacts = populateContactArray(limit, populate, c);

            logger.log("Contacts found: ", contacts.toString());
            EventQueue.pushEvent(new ContactFound("", contacts.toString(), token));
        } catch (FileNotFoundException e) {
            EventQueue.pushEvent(new ContactFound("FileNotFoundException:" + e.getMessage()+ token +" [[STACK]] :"+stackTraceToString(e), "", token));
            logger.log(e);
            return;
        } catch (IOException e) {
            EventQueue.pushEvent(new ContactFound("IOException:" + e.getMessage()+ token +" [[STACK]] :"+stackTraceToString(e), "", token));
            logger.log(e);
            return;
        } catch (JSONException e) {
            EventQueue.pushEvent(new ContactFound("JSONException:" + e.getMessage()+ token +" [[STACK]] :"+stackTraceToString(e), "", token));
            logger.log(e);
            return;
        }
    }

    public void getFavourite(String jsonData) {
        logger.log("getFavourite contacts : "+ jsonData);
        String token = "";
        try {
            String[] projection = new String[] {
                    ContactsContract.Contacts._ID,
                    ContactsContract.Contacts.DISPLAY_NAME
            };
            Cursor cursor = _activity.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                    projection,
                    "starred=?",
                    new String[] {"1"}, null);

            JSONArray contacts = new JSONArray();
            int colContactId = cursor.getColumnIndex(ContactsContract.Contacts._ID);
            int colDisplayName = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);

            int colPhoneNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

            token = new JSONObject(jsonData).optString("token");
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext() ) {
                    JSONObject contact = new JSONObject();

                    contact.put("id", cursor.getString(colContactId));
                    contact.put("displayName", cursor.getString(colDisplayName));
                    contact.put("photo", photoQuery(cursor, cursor.getString(colContactId)));
                    contacts.put(contact);
                }
            }

            cursor.close();
            logger.log("Favourite contacts:" + contacts.toString());
            EventQueue.pushEvent(new ContactFound("", contacts.toString(), token));
        } catch (JSONException e) {
            EventQueue.pushEvent(new ContactFound("JSONException:" + e.getMessage()+ token +" [[STACK]] :"+stackTraceToString(e), "", token));
            logger.log(e);
            return;
        } catch (FileNotFoundException e) {
            EventQueue.pushEvent(new ContactFound("FileNotFoundException:" + e.getMessage()+ token +" [[STACK]] :"+stackTraceToString(e), "", token));
            logger.log(e);
            return;
        } catch (IOException e) {
            EventQueue.pushEvent(new ContactFound("IOException:" + e.getMessage()+ token +" [[STACK]] :"+stackTraceToString(e), "", token));
            logger.log(e);
            return;
        }
    }

    public void getAll(String jsonData) {
        logger.debug(" getAll contacts:"+ jsonData);
        String token = "";
        try {
            JSONArray desiredFields = new JSONArray();
            JSONObject options = new JSONObject();
            JSONObject inputObj = new JSONObject(jsonData);

            desiredFields.put("displayName");
            desiredFields.put("name");
            desiredFields.put("emails");
            desiredFields.put("phoneNumbers");
            desiredFields.put("ims");
            token = inputObj.optString("token");
            options.put("desiredFields", desiredFields);

            HashMap<String, Boolean> populate = buildPopulationSet(options);
            HashSet<String> columnsToFetch = getColumnsToFetch(populate);

            Cursor c = _activity.getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                    columnsToFetch.toArray(new String[] {}),
                    null,
                    null,
                    ContactsContract.Data.CONTACT_ID + " ASC");

            int limit = Integer.MAX_VALUE;
            JSONArray contacts = populateContactArray(limit, populate, c);
            logger.log("All valid contacts count:" + contacts.length());
            EventQueue.pushEvent(new ContactFound("", contacts.toString(), token ));
        } catch (JSONException e) {
            EventQueue.pushEvent(new ContactFound("JSONException:" + e.getMessage()+ token +" [[STACK]] :"+stackTraceToString(e), "", token));
            logger.log(e);
            return;
        } catch (FileNotFoundException e) {
            EventQueue.pushEvent(new ContactFound("FileNotFoundException:" + e.getMessage()+ token +" [[STACK]] :"+stackTraceToString(e), "", token));
            logger.log(e);
            return;
        } catch (IOException e) {
            EventQueue.pushEvent(new ContactFound("IOException:" + e.getMessage()+ token +" [[STACK]] :"+stackTraceToString(e), "", token));
            logger.log(e);
            return;
        }
    }
    private HashSet<String> getColumnsToFetch(HashMap<String, Boolean> populate) {
        HashSet<String> columnsToFetch = new HashSet<String>();
        columnsToFetch.add(ContactsContract.Data.CONTACT_ID);
        columnsToFetch.add(ContactsContract.Data.RAW_CONTACT_ID);
        columnsToFetch.add(ContactsContract.Data.MIMETYPE);

        if (isRequired("displayName", populate)) {
            columnsToFetch.add(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME);
        }
        if (isRequired("name", populate)) {
            columnsToFetch.add(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME);
            columnsToFetch.add(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME);
            columnsToFetch.add(ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME);
            columnsToFetch.add(ContactsContract.CommonDataKinds.StructuredName.PREFIX);
            columnsToFetch.add(ContactsContract.CommonDataKinds.StructuredName.SUFFIX);
        }
        if (isRequired("phoneNumbers", populate)) {
            columnsToFetch.add(ContactsContract.CommonDataKinds.Phone._ID);
            columnsToFetch.add(ContactsContract.CommonDataKinds.Phone.NUMBER);
            columnsToFetch.add(ContactsContract.CommonDataKinds.Phone.TYPE);
        }
        if (isRequired("emails", populate)) {
            columnsToFetch.add(ContactsContract.CommonDataKinds.Email._ID);
            columnsToFetch.add(ContactsContract.CommonDataKinds.Email.DATA);
            columnsToFetch.add(ContactsContract.CommonDataKinds.Email.TYPE);
        }
        if (isRequired("addresses", populate)) {
            columnsToFetch.add(ContactsContract.CommonDataKinds.StructuredPostal._ID);
            columnsToFetch.add(ContactsContract.CommonDataKinds.Organization.TYPE);
            columnsToFetch.add(ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS);
            columnsToFetch.add(ContactsContract.CommonDataKinds.StructuredPostal.STREET);
            columnsToFetch.add(ContactsContract.CommonDataKinds.StructuredPostal.CITY);
            columnsToFetch.add(ContactsContract.CommonDataKinds.StructuredPostal.REGION);
            columnsToFetch.add(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE);
            columnsToFetch.add(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY);
        }
        if (isRequired("organizations", populate)) {
            columnsToFetch.add(ContactsContract.CommonDataKinds.Organization._ID);
            columnsToFetch.add(ContactsContract.CommonDataKinds.Organization.TYPE);
            columnsToFetch.add(ContactsContract.CommonDataKinds.Organization.DEPARTMENT);
            columnsToFetch.add(ContactsContract.CommonDataKinds.Organization.COMPANY);
            columnsToFetch.add(ContactsContract.CommonDataKinds.Organization.TITLE);
        }
        if (isRequired("ims", populate)) {
            columnsToFetch.add(ContactsContract.CommonDataKinds.Im._ID);
            columnsToFetch.add(ContactsContract.CommonDataKinds.Im.DATA);
            columnsToFetch.add(ContactsContract.CommonDataKinds.Im.TYPE);
        }
        if (isRequired("note", populate)) {
            columnsToFetch.add(ContactsContract.CommonDataKinds.Note.NOTE);
        }
        if (isRequired("nickname", populate)) {
            columnsToFetch.add(ContactsContract.CommonDataKinds.Nickname.NAME);
        }
        if (isRequired("urls", populate)) {
            columnsToFetch.add(ContactsContract.CommonDataKinds.Website._ID);
            columnsToFetch.add(ContactsContract.CommonDataKinds.Website.URL);
            columnsToFetch.add(ContactsContract.CommonDataKinds.Website.TYPE);
        }
        if (isRequired("birthday", populate)) {
            columnsToFetch.add(ContactsContract.CommonDataKinds.Event.START_DATE);
            columnsToFetch.add(ContactsContract.CommonDataKinds.Event.TYPE);
        }
        if (isRequired("photos", populate)) {
            columnsToFetch.add(ContactsContract.CommonDataKinds.Photo._ID);
        }
        return columnsToFetch;
    }
    private WhereOptions buildIdClause(Set<String> contactIds, String searchTerm) {
        WhereOptions options = new WhereOptions();

        // If the user is searching for every contact then short circuit the method
        // and return a shorter where clause to be searched.
        if (searchTerm.equals("%")) {
            options.setWhere("(" + ContactsContract.Data.CONTACT_ID + " LIKE ? )");
            options.setWhereArgs(new String[] { searchTerm });
            return options;
        }

        // This clause means that there are specific ID's to be populated
        Iterator<String> it = contactIds.iterator();
        StringBuffer buffer = new StringBuffer("(");

        while (it.hasNext()) {
            buffer.append("'" + it.next() + "'");
            if (it.hasNext()) {
                buffer.append(",");
            }
        }
        buffer.append(")");

        options.setWhere(ContactsContract.Data.CONTACT_ID + " IN " + buffer.toString());
        options.setWhereArgs(null);

        return options;
    }

    private JSONObject populateContact(JSONObject contact, JSONArray phones, JSONArray emails, JSONArray ims,
            JSONArray photos) throws JSONException {
        if (phones.length() > 0) {
            contact.put("phoneNumbers", phones);
        }
        if (emails.length() > 0) {
            contact.put("emails", emails);
        }
        if (ims.length() > 0) {
            contact.put("ims", ims);
        }
        if (photos.length() > 0) {
            contact.put("photos", photos);
        }
        return contact;
    }

    /**
     * Take the search criteria passed into the method and create a SQL WHERE clause.
     * @param fields the properties to search against
     * @param searchTerm the string to search for
     * @return an object containing the selection and selection args
     * @throws JSONException
     */
    private WhereOptions buildWhereClause(JSONArray fields, String searchTerm) throws JSONException {

        ArrayList<String> where = new ArrayList<String>();
        ArrayList<String> whereArgs = new ArrayList<String>();

        WhereOptions options = new WhereOptions();

        /*
         * Special case where the user wants all fields returned
         */
        if (isWildCardSearch(fields)) {
            // Get all contacts with all properties
            if ("%".equals(searchTerm)) {
                options.setWhere("(" + ContactsContract.Contacts.DISPLAY_NAME + " LIKE ? )");
                options.setWhereArgs(new String[] { searchTerm });
                return options;
            } else {
                // Get all contacts that match the filter but return all properties
                where.add("(" + dbMap.get("displayName") + " LIKE ? )");
                whereArgs.add(searchTerm);
                where.add("(" + dbMap.get("name") + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
                where.add("(" + dbMap.get("nickname") + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE);
                where.add("(" + dbMap.get("phoneNumbers") + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                where.add("(" + dbMap.get("emails") + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE);
                where.add("(" + dbMap.get("addresses") + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE);
                where.add("(" + dbMap.get("ims") + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE);
                where.add("(" + dbMap.get("organizations") + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE);
                where.add("(" + dbMap.get("note") + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE);
                where.add("(" + dbMap.get("urls") + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE);
            }
        }

        /*
         * Special case for when the user wants all the contacts but
         */
        if ("%".equals(searchTerm)) {
            options.setWhere("(" + ContactsContract.Contacts.DISPLAY_NAME + " LIKE ? )");
            options.setWhereArgs(new String[] { searchTerm });
            return options;
        }

        String key;
        //Log.d(LOG_TAG, "How many fields do we have = " + fields.length());
        for (int i = 0; i < fields.length(); i++) {
            key = fields.getString(i);

            if (key.equals("id")) {
                where.add("(" + dbMap.get(key) + " = ? )");
                whereArgs.add(searchTerm.substring(1, searchTerm.length() - 1));
            }
            else if (key.startsWith("displayName")) {
                where.add("(" + dbMap.get(key) + " LIKE ? )");
                whereArgs.add(searchTerm);
            }
            else if (key.startsWith("name")) {
                where.add("(" + dbMap.get(key) + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
            }
            else if (key.startsWith("nickname")) {
                where.add("(" + dbMap.get(key) + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE);
            }
            else if (key.startsWith("phoneNumbers")) {
                where.add("(" + dbMap.get(key) + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
            }
            else if (key.startsWith("emails")) {
                where.add("(" + dbMap.get(key) + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE);
            }
            else if (key.startsWith("addresses")) {
                where.add("(" + dbMap.get(key) + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE);
            }
            else if (key.startsWith("ims")) {
                where.add("(" + dbMap.get(key) + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE);
            }
            else if (key.startsWith("organizations")) {
                where.add("(" + dbMap.get(key) + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE);
            }
            //        else if (key.startsWith("birthday")) {
            //          where.add("(" + dbMap.get(key) + " LIKE ? AND "
            //              + ContactsContract.Data.MIMETYPE + " = ? )");
            //        }
            else if (key.startsWith("note")) {
                where.add("(" + dbMap.get(key) + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE);
            }
            else if (key.startsWith("urls")) {
                where.add("(" + dbMap.get(key) + " LIKE ? AND "
                        + ContactsContract.Data.MIMETYPE + " = ? )");
                whereArgs.add(searchTerm);
                whereArgs.add(ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE);
            }
        }

        // Creating the where string
        StringBuffer selection = new StringBuffer();
        for (int i = 0; i < where.size(); i++) {
            selection.append(where.get(i));
            if (i != (where.size() - 1)) {
                selection.append(" OR ");
            }
        }
        options.setWhere(selection.toString());

        // Creating the where args array
        String[] selectionArgs = new String[whereArgs.size()];
        for (int i = 0; i < whereArgs.size(); i++) {
            selectionArgs[i] = whereArgs.get(i);
        }
        options.setWhereArgs(selectionArgs);

        return options;
    }
    protected HashMap<String, Boolean> buildPopulationSet(JSONObject options) throws JSONException {
        HashMap<String, Boolean> map = new HashMap<String, Boolean>();

        String key;
        JSONArray desiredFields = null;
        if (options!=null && options.has("desiredFields")) {
            desiredFields = options.getJSONArray("desiredFields");
        }
        if (desiredFields == null || desiredFields.length() == 0) {
            map.put("displayName", true);
            map.put("name", true);
            //map.put("nickname", true);
            map.put("phoneNumbers", true);
            map.put("emails", true);
            //map.put("addresses", true);
            map.put("ims", true);
            /*map.put("organizations", true);
            map.put("birthday", true);
            map.put("note", true);
            map.put("urls", true);*/
            map.put("photos", true);
            //map.put("categories", true);
        } else {
            for (int i = 0; i < desiredFields.length(); i++) {
                key = desiredFields.getString(i);
                if (key.startsWith("displayName")) {
                    map.put("displayName", true);
                } else if (key.startsWith("name")) {
                    map.put("displayName", true);
                    map.put("name", true);
                } else if (key.startsWith("nickname")) {
                    map.put("nickname", true);
                } else if (key.startsWith("phoneNumbers")) {
                    map.put("phoneNumbers", true);
                } else if (key.startsWith("emails")) {
                    map.put("emails", true);
                } else if (key.startsWith("addresses")) {
                    map.put("addresses", true);
                } else if (key.startsWith("ims")) {
                    map.put("ims", true);
                } else if (key.startsWith("organizations")) {
                    map.put("organizations", true);
                } else if (key.startsWith("birthday")) {
                    map.put("birthday", true);
                } else if (key.startsWith("note")) {
                    map.put("note", true);
                } else if (key.startsWith("urls")) {
                    map.put("urls", true);
                } else if (key.startsWith("photos")) {
                    map.put("photos", true);
                } else if (key.startsWith("categories")) {
                    map.put("categories", true);
                }
            }
        }
        return map;
    }
    protected boolean isRequired(String key, HashMap<String,Boolean> map) {
        Boolean retVal = map.get(key);
        return (retVal == null) ? false : retVal.booleanValue();
    }
    private JSONArray populateContactArray(int limit,
            HashMap<String, Boolean> populate, Cursor c) throws JSONException, FileNotFoundException, IOException {

        logger.log("Inside populateContactArray");
        String contactId = "";
        String rawId = "";
        String oldContactId = "";
        boolean newContact = true;
        String mimetype = "";

        JSONArray contacts = new JSONArray();
        JSONObject contact = new JSONObject();
        JSONArray organizations = new JSONArray();
        JSONArray addresses = new JSONArray();
        JSONArray phones = new JSONArray();
        JSONArray emails = new JSONArray();
        JSONArray validPhones = new JSONArray();
        JSONArray ims = new JSONArray();
        JSONArray websites = new JSONArray();
        JSONArray photos = new JSONArray();

        // Column indices
        int colContactId = c.getColumnIndex(ContactsContract.Data.CONTACT_ID);
        int colRawContactId = c.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID);
        int colMimetype = c.getColumnIndex(ContactsContract.Data.MIMETYPE);
        int colDisplayName = c.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME);
        int colNote = c.getColumnIndex(ContactsContract.CommonDataKinds.Note.NOTE);
        int colNickname = c.getColumnIndex(ContactsContract.CommonDataKinds.Nickname.NAME);
        int colBirthday = c.getColumnIndex(ContactsContract.CommonDataKinds.Event.START_DATE);
        int colEventType = c.getColumnIndex(ContactsContract.CommonDataKinds.Event.TYPE);
        int colPhoneNumbers = c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

        if (c.getCount() > 0) {
            while (c.moveToNext() && (contacts.length() <= (limit - 1))) {
                contactId = c.getString(colContactId);
                rawId = c.getString(colRawContactId);

                // If we are in the first row set the oldContactId
                if (c.getPosition() == 0) {
                    oldContactId = contactId;
                }

                // When the contact ID changes we need to push the Contact object
                // to the array of contacts and create new objects.
                if (!oldContactId.equals(contactId)) {
                    // Populate the Contact object with it's arrays
                    // and push the contact into the contacts array

                    boolean isValid = false;
                    for (int i = 0; i < phones.length(); i++) {
                        String phone = phones.optString(i);
                        if(phone != null && phone.length() > 6){
                            isValid = true;
                            validPhones.put(phones.get(i));
                        }else{
                            isValid = false;
                        }
                    }
                    if (validPhones.length() > 0) {
                        contacts.put(populateContact(contact, validPhones, emails, ims,photos));
                    }

                    // Clean up the objects
                    contact = new JSONObject();
                    phones = new JSONArray();
                    validPhones = new JSONArray();
                    emails = new JSONArray();
                    ims = new JSONArray();
                    photos = new JSONArray();

                    // Set newContact to true as we are starting to populate a new contact
                    newContact = true;
                }

                // When we detect a new contact set the ID and display name.
                // These fields are available in every row in the result set returned.
                if (newContact) {
                    newContact = false;
                    contact.put("id", contactId);
                    contact.put("rawId", rawId);
                }

                // Grab the mimetype of the current row as it will be used in a lot of comparisons
                mimetype = c.getString(colMimetype);

                if (mimetype.equals(ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE) && isRequired("name", populate)) {
                    contact.put("displayName", c.getString(colDisplayName));
                }

                if (mimetype.equals(ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                        && isRequired("name", populate)) {
                    //                      contact.put("name", nameQuery(c));
                }
                else if (mimetype.equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                        && isRequired("phoneNumbers", populate)) {
                    String e164Formatted = formatPhoneNumberToE164(c.getString(colPhoneNumbers));
                    if(e164Formatted != null && !e164Formatted.isEmpty()) {
                        phones.put(e164Formatted);
                    }
                }
                else if (mimetype.equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                        && isRequired("emails", populate)) {
                                          emails.put(emailQuery(c));
                }
                else if (mimetype.equals(ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE)
                        && isRequired("ims", populate)) {
                                          ims.put(imQuery(c));
                }
                else if (mimetype.equals(ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE)
                        && isRequired("note", populate)) {
                    contact.put("note", c.getString(colNote));
                }
                else if (mimetype.equals(ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE)
                        && isRequired("nickname", populate)) {
                    contact.put("nickname", c.getString(colNickname));
                }
                else if (mimetype.equals(ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE)
                        && isRequired("urls", populate)) {
                    //                      websites.put(websiteQuery(c));
                }
                else if (mimetype.equals(ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE)) {
                    if (isRequired("birthday", populate) &&
                            ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY == c.getInt(colEventType)) {
                        contact.put("birthday", c.getString(colBirthday));
                    }
                }
                else if (mimetype.equals(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                        && isRequired("photos", populate)) {
                    photos.put(photoQuery(c, contactId));
                }

                // Set the old contact ID
                oldContactId = contactId;

            }

            // Push the last contact into the contacts array
            if (contacts.length() < limit) {
                contacts.put(populateContact(contact, validPhones, emails, ims,photos));
            }
        }
        c.close();
        return contacts;
    }

    private String formatPhoneNumberToE164 (String storedNumber) {
        String e164Formatted = "";
        try {
            String isoCountryCode = null;
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            TelephonyManager telephonyManager =(TelephonyManager)_ctx.getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
                //Not sure if this would work in all scenarios.
                isoCountryCode = telephonyManager.getSimCountryIso().toUpperCase();
            } else {
                isoCountryCode = telephonyManager.getNetworkCountryIso().toUpperCase();
            }
            logger.debug("Stored number:" + storedNumber);
            logger.debug("ISO Country code:" + isoCountryCode);
            PhoneNumber phoneNumber = phoneUtil.parse(storedNumber, isoCountryCode);
            e164Formatted  = phoneUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
            logger.debug("Phone number E164 format: " + e164Formatted);
        } catch (NumberParseException e) {
            logger.log("NumberParseException was thrown for " + storedNumber+ ": " + e.getMessage()+" [[STACK]] :"+ stackTraceToString(e));
        }
        return e164Formatted;
    }

    private JSONObject photoQuery(Cursor cursor, String contactId) throws JSONException, FileNotFoundException, IOException {
        JSONObject photo = new JSONObject();
        photo.put("id", cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Photo._ID)));
        photo.put("pref", false);
        photo.put("type", "url");
        String personImage = getPhotoFromDB(contactId);
        photo.put("value", personImage);
        return photo;
    }

    private String getPhotoFromDB(String contactId) throws FileNotFoundException, IOException {
        logger.log("Inside getPhotoFromDB: Person contact id : "+ contactId);
        Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, (Long.valueOf(contactId)));
        InputStream stream = ContactsContract.Contacts.openContactPhotoInputStream(_activity.getContentResolver(), person);
        String personImage = "";
        if (null != stream) {
            personImage = "PH_"+contactId+".JPEG";
            if(!Arrays.asList(_activity.fileList()).contains(personImage)){
                FileOutputStream fos = _activity.openFileOutput(personImage, Context.MODE_PRIVATE);
                BitmapFactory.decodeStream(stream).compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();
                logger.log("Photo copied to directory  : "+ _activity.getFilesDir());
            }
        }
        return personImage;
    }

    private boolean isWildCardSearch(JSONArray fields) throws JSONException {
        // Only do a wildcard search if we are passed ["*"]
        if (fields.length() == 1) {
            if ("*".equals(fields.getString(0))) {
                return true;
            }
        }
        return false;
    }

    private static String stackTraceToString(Throwable e) {
        StringBuilder sb = new StringBuilder();
        for (StackTraceElement element : e.getStackTrace()) {
            sb.append(element.toString());
            sb.append("\n");
        }
        return sb.toString();
    }


    /**
     * Create a ContactField JSONObject
     * @param cursor the current database row
     * @return a JSONObject representing a ContactField
     */
    private JSONObject emailQuery(Cursor cursor) throws JSONException {
        JSONObject email = new JSONObject();
        email.put("id", cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email._ID)));
        email.put("pref", false); // Android does not store pref attribute
        email.put("value", cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)));
        email.put("type", getContactType(cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE))));
        return email;
    }

    /**
     * Create a ContactField JSONObject
     * @param cursor the current database row
     * @return a JSONObject representing a ContactField
     */
     private JSONObject imQuery(Cursor cursor) throws JSONException {
        JSONObject im = new JSONObject();
        im.put("id", cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Im._ID)));
        im.put("pref", false); // Android does not store pref attribute
        im.put("value", cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Im.DATA)));
        im.put("type", getImType(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Im.PROTOCOL))));
        return im;
    }

     /**
     * Converts a string from the W3C Contact API to it's Android int value.
     * @param string
     * @return Android int value
     */
    private int getImType(String string) {
        int type = ContactsContract.CommonDataKinds.Im.PROTOCOL_CUSTOM;
        if (string != null) {
            if ("aim".equals(string.toLowerCase())) {
                return ContactsContract.CommonDataKinds.Im.PROTOCOL_AIM;
            }
            else if ("google talk".equals(string.toLowerCase())) {
                return ContactsContract.CommonDataKinds.Im.PROTOCOL_GOOGLE_TALK;
            }
            else if ("icq".equals(string.toLowerCase())) {
                return ContactsContract.CommonDataKinds.Im.PROTOCOL_ICQ;
            }
            else if ("jabber".equals(string.toLowerCase())) {
                return ContactsContract.CommonDataKinds.Im.PROTOCOL_JABBER;
            }
            else if ("msn".equals(string.toLowerCase())) {
                return ContactsContract.CommonDataKinds.Im.PROTOCOL_MSN;
            }
            else if ("netmeeting".equals(string.toLowerCase())) {
                return ContactsContract.CommonDataKinds.Im.PROTOCOL_NETMEETING;
            }
            else if ("qq".equals(string.toLowerCase())) {
                return ContactsContract.CommonDataKinds.Im.PROTOCOL_QQ;
            }
            else if ("skype".equals(string.toLowerCase())) {
                return ContactsContract.CommonDataKinds.Im.PROTOCOL_SKYPE;
            }
            else if ("yahoo".equals(string.toLowerCase())) {
                return ContactsContract.CommonDataKinds.Im.PROTOCOL_YAHOO;
            }
        }
        return type;
    }

    /**
     * Converts a string from the W3C Contact API to it's Android int value.
     * @param string
     * @return Android int value
     */
    private int getContactType(String string) {
        int type = ContactsContract.CommonDataKinds.Email.TYPE_OTHER;
        if (string != null) {
            if ("home".equals(string.toLowerCase())) {
                return ContactsContract.CommonDataKinds.Email.TYPE_HOME;
            }
            else if ("work".equals(string.toLowerCase())) {
                return ContactsContract.CommonDataKinds.Email.TYPE_WORK;
            }
            else if ("other".equals(string.toLowerCase())) {
                return ContactsContract.CommonDataKinds.Email.TYPE_OTHER;
            }
            else if ("mobile".equals(string.toLowerCase())) {
                return ContactsContract.CommonDataKinds.Email.TYPE_MOBILE;
            }
            else if ("custom".equals(string.toLowerCase())) {
                return ContactsContract.CommonDataKinds.Email.TYPE_CUSTOM;
            }
        }
        return type;
    }

    /**
     * getPhoneType converts an Android phone type into a string
     * @param type
     * @return phone type as string.
     */
    private String getContactType(int type) {
        String stringType;
        switch (type) {
        case ContactsContract.CommonDataKinds.Email.TYPE_CUSTOM:
            stringType = "custom";
            break;
        case ContactsContract.CommonDataKinds.Email.TYPE_HOME:
            stringType = "home";
            break;
        case ContactsContract.CommonDataKinds.Email.TYPE_WORK:
            stringType = "work";
            break;
        case ContactsContract.CommonDataKinds.Email.TYPE_MOBILE:
            stringType = "mobile";
            break;
        case ContactsContract.CommonDataKinds.Email.TYPE_OTHER:
        default:
            stringType = "other";
            break;
        }
        return stringType;
    }
}


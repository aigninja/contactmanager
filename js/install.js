(function (global) {
    var callbackStore = {},
        apis = 'search getFavourite getAll'.split(' ');

    global.contactManager = {};

    function createAPI(api) {
        return function findContact(input, callback) {
            if (typeof input === "object") {
                input.token = Date.now() + Math.random().toString(36).substr(2, 9);
            }
            logger.log('Making a call to ' + api + ' API of ContactManager with input ' + JSON.stringify(input));
            NATIVE.plugins.sendEvent("ContactManager", api, typeof input === "object" ? JSON.stringify(input) : "{}");
            if (typeof callback === "function") {
                callbackStore[input.token] = callback;
            }
        };
    }

    apis.forEach(function (api) {
        global.contactManager[api] = createAPI(api);
    });

    NATIVE.events.registerHandler('contactFound', function (response) {
        logger.log("In response handler, response: " + JSON.stringify(response));
        if (response.errorMessage) {
            callbackStore[response.token](new Error(response.errorMessage));
        } else {
            callbackStore[response.token](null, response.contacts);
        }
        delete callbackStore[response.token];
    });


}(GLOBAL));
